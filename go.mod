module gitlab.com/your_friend_alice/streamrecord

go 1.14

require (
	github.com/lestrrat-go/strftime v1.0.4
	github.com/spf13/cobra v1.1.3
	github.com/tcolgate/mp3 v0.0.0-20170426193717-e79c5a46d300
)

replace github.com/spf13/cobra => github.com/your-friend-alice/cobra v1.1.3-noviper
