package downloader

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/tcolgate/mp3"
	"log"
	"net/http"
	"time"
)

type Downloader struct {
	Url           string
	Timeout       time.Duration
	client        http.Client
	response      *http.Response
	cancelRequest context.CancelFunc
	lastAttempt   time.Time
	// Total duration of the mp3 frames returned by the current request
	mp3Duration time.Duration
}

func New(url string, verify bool, timeout time.Duration) *Downloader {
	d := &Downloader{
		Url:     url,
		Timeout: timeout,
		client: http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: !verify},
		}},
	}
	return d
}

// Block until d.Timeout has elapsed since the last request was opened, to avoid spamming.
func (d *Downloader) wait() {
	sleepTime := d.lastAttempt.Add(d.Timeout).Sub(time.Now())
	if sleepTime > 0 {
		go log.Printf("Retrying in %s", sleepTime)
		time.Sleep(sleepTime)
	}
}

// Open a new request
func (d *Downloader) openNewRequest() (err error) {
	d.client.CloseIdleConnections()
	d.lastAttempt = time.Now()
	var ctx context.Context
	ctx, d.cancelRequest = context.WithCancel(context.Background())
	var request *http.Request
	request, err = http.NewRequestWithContext(ctx, "GET", d.Url, nil)
	if err != nil {
		err = fmt.Errorf("Error preparing request:\n  %w", err)
		return
	}
	d.response, err = d.client.Do(request)
	if err != nil {
		err = fmt.Errorf("Error sending request:\n  %w", err)
	}
	return
}

type result struct {
	frame *mp3.Frame
	err   error
}

// Return an mp3 frame. If the context expires, the current request will be terminated and nothing will be returned.
func (d *Downloader) getFrame(ctx context.Context) (frame *mp3.Frame, err error) {
	if d.response == nil {
		err = d.openNewRequest()
		if err != nil {
			return
		}
	}
	results := make(chan result, 1)
	decoder := mp3.NewDecoder(d.response.Body)
	go func() {
		// read frame from request repsonse, push it or the error into the channel
		f := &mp3.Frame{}
		skipped := 0
		err := decoder.Decode(f, &skipped)
		if err != nil {
			results <- result{err: err}
		} else {
			results <- result{frame: f}
		}
		close(results)
	}()
	select {
	case r := <-results:
		if r.err != nil {
			err = r.err
		} else if r.frame != nil {
			frame = r.frame
		}
	case <-ctx.Done():
		d.response.Body.Close()
		d.cancelRequest()
		d.response = nil
		err = ctx.Err()
	}
	return
}

func (d *Downloader) GetFrame() (frame *mp3.Frame) {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), d.Timeout)
		defer cancel()
		frame, err := d.getFrame(ctx)
		if err == nil && frame != nil {
			d.mp3Duration += frame.Duration()
			return frame
		} else {
			log.Println(fmt.Errorf("Error fetching mp3 frame:\n  %w", err))
			if d.response != nil {
				d.response.Body.Close()
			}
			d.response = nil
			d.wait()
		}
	}
}

func (d *Downloader) Lag() time.Duration {
	return time.Now().Sub(d.lastAttempt) - d.mp3Duration
}
