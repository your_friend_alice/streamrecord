package saver

import (
	"fmt"
	"github.com/lestrrat-go/strftime"
	"github.com/tcolgate/mp3"
	"gitlab.com/your_friend_alice/streamrecord/pkg/downloader"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"
)

type Saver struct {
	Period        time.Duration
	MaxSplitDelay time.Duration
	Downloader    *downloader.Downloader
	Format        *strftime.Strftime
	Mkdir         bool
}

func (s Saver) shouldSplit(current time.Time, frame *mp3.Frame) bool {
	if frame == nil {
		return false
	}
	now := time.Now()
	next := nextSegment(current, s.Period)
	if now.Before(next) {
		return false
	}
	if now.After(next.Add(s.MaxSplitDelay)) {
		return true
	}
	return frame.SideInfo().NDataBegin() == 0
}

func (s Saver) Run() error {
	for {
		current := currentSegment(s.Period)
		filename := s.Format.FormatString(current)
		log.Printf("Opening file %q", filename)
		err := os.MkdirAll(filepath.Dir(filename), 0755)
		if err != nil {
			return fmt.Errorf("Error creating directory %q:\n  %w", filepath.Dir(filename), err)
		}
		file, err := os.OpenFile(
			filename,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY,
			0644,
		)
		if err != nil {
			file.Close()
			return fmt.Errorf("Error opening file %q:\n  %w", filename, err)
		}
		var frame *mp3.Frame
		duration := time.Duration(0)
		for !s.shouldSplit(current, frame) {
			frame = s.Downloader.GetFrame()
			duration += frame.Duration()
			_, err := io.Copy(file, frame.Reader())
			if err != nil {
				file.Close()
				return fmt.Errorf("Error writing mp3 frame to file %q:\n  %w", filename, err)
			}
		}
		log.Printf("Closing file %q after writing %s of audio", filename, duration)
		file.Close()
	}
}

func today(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func currentSegment(period time.Duration) time.Time {
	now := time.Now()
	today := today(now)
	timeOfDay := (now.Sub(today) / period) * period
	return today.Add(timeOfDay)
}

func nextSegment(current time.Time, period time.Duration) time.Time {
	next := current.Add(period)
	nextDay := today(next)
	if today(current) == nextDay {
		return next
	} else {
		return nextDay
	}
}
