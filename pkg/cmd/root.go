package cmd

import (
	"fmt"
	"github.com/lestrrat-go/strftime"
	"github.com/spf13/cobra"
	"gitlab.com/your_friend_alice/streamrecord/pkg/downloader"
	"gitlab.com/your_friend_alice/streamrecord/pkg/saver"
	"os"
	"strings"
	"time"
)

var Insecure bool
var ignoreY2k bool
var Period time.Duration
var MaxSplitDelay time.Duration
var template string
var Format *strftime.Strftime
var Mkdir bool
var Timeout time.Duration
var MaxLag time.Duration

var rootCmd = &cobra.Command{
	Use:          "streamrecord",
	Short:        "Capture an HTTP/HTTPS MP3 stream to a series of MP3 files.",
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		var err error
		Format, err = strftime.New(template, strftime.WithUnixSeconds('s'))
		if err != nil {
			return fmt.Errorf("Error parsing template %s:\n  %w", template, err)
		}
		if err = validate(); err != nil {
			return err
		}
		downloader := downloader.New(args[0], !Insecure, Timeout)
		saver := saver.Saver{
			Period:        Period,
			MaxSplitDelay: MaxSplitDelay,
			Downloader:    downloader,
			Format:        Format,
			Mkdir:         Mkdir,
		}
		return saver.Run()
	},
}

func validate() error {
	if (time.Hour*24)%Period != 0 {
		return fmt.Errorf("Period %s is not an integer factor of %s", Period, time.Hour*24)
	} else if Period < time.Second {
		return fmt.Errorf("Period %s must be at least %s", Period, time.Second/10)
	} else if !Mkdir && strings.Contains(template, "/") {
		return fmt.Errorf("Slashes are not allowed in the --template value unless --create-directories is set")
	} else if !Mkdir && strings.Contains(template, "%D") {
		return fmt.Errorf("Slashes are not allowed in the file name unless --create-directories is set, and %%D generates slashes")
	} else if !uniqueTemplate(Format, Period, !ignoreY2k) {
		return fmt.Errorf("Template %s does not produce unique values for every %s", template, Period)
	}
	return nil
}

func init() {
	rootCmd.Flags().BoolVarP(&Insecure, "insecure-skip-verify", "k", false, "Accept invalid HTTPS certificates")
	rootCmd.Flags().BoolVarP(&ignoreY2k, "ignore-y2k", "", false, "Ignore centuries when validating template strings")
	rootCmd.Flags().DurationVarP(&Period, "duration", "d", time.Hour, "Duration of each segment. Must be an integer factor of a day. Segments start at integer multiples of the segment duration after midnight of each day, meaning a 1 hour segment length will start on the hour, regardless of when recording was started. If recording is started midway through a segment's time slot, it will record what it can to the time slot's file.")
	rootCmd.Flags().DurationVarP(&Period, "max-split-delay", "s", time.Millisecond*200, "Maximum amount of time to wait after the correct split time before actually splitting. MP3 frames can use data from a previous frame using a mechanism called the \"bit reservoir\". In order to cleanly split an mp3 file and avoid glitches, you need to wait for a frame that doesn't use any data from a previous frame. Streamrecord will try to split the file cleanly, but if --max-split-delay has elapsed and no clean place to split has been found, it will give up and produce an unclean break.")
	rootCmd.Flags().StringVarP(&template, "template", "f", "%FT%T.mp3", "Template for the filename, using standard strftime formatting. Must produce a unique value for every possible valid segment.")
	rootCmd.Flags().BoolVarP(&Mkdir, "create-directories", "c", false, "Allow slashes in the template, and create directories accordingly.")
	rootCmd.Flags().DurationVarP(&Timeout, "timeout", "t", 2*time.Second, "Set HTTP connect, receive timeouts.")
	rootCmd.Flags().DurationVarP(&MaxLag, "max-lag", "l", 10*time.Second, "Maximum lag, measured as the difference between the duration of the received mp3 frames and the duration of the HTTP response. If the maximum lag is exceeded, the connection will be closed and a new one will be opened.")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func uniqueTemplate(format *strftime.Strftime, period time.Duration, y2k bool) bool {
	outputSet := map[string]struct{}{}
	halfDay := ((12 * time.Hour) / period) * period
	dates := map[time.Time]struct{}{
		time.Date(1999, time.January, 1, 0, 0, 0, 0, time.UTC):                           struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC):                           struct{}{},
		time.Date(2000, time.January, 2, 0, 0, 0, 0, time.UTC):                           struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Add(period):               struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Add(-period):              struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Add(halfDay):              struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Add(halfDay).Add(period):  struct{}{},
		time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC).Add(halfDay).Add(-period): struct{}{},
		time.Date(2000, time.February, 1, 0, 0, 0, 0, time.UTC):                          struct{}{},
		time.Date(2000, time.February, 8, 0, 0, 0, 0, time.UTC):                          struct{}{},
		time.Date(2000, time.February, 1, 0, 0, 0, 0, time.UTC).Add(-period):             struct{}{},
	}
	if y2k {
		dates[time.Date(2099, time.January, 1, 0, 0, 0, 0, time.UTC)] = struct{}{}
	}
	for date, _ := range dates {
		outputSet[format.FormatString(date)] = struct{}{}
	}
	return len(outputSet) == len(dates)
}
