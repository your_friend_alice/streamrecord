# streamrecord

Capture an HTTP/HTTPS MP3 stream to a series of MP3 files.

```
streamrecord --help
Capture an HTTP/HTTPS MP3 stream to a series of MP3 files.

Usage:
  streamrecord [flags]

Flags:
  -c, --create-directories         Allow slashes in the template, and create directories accordingly.
  -d, --duration duration          Duration of each segment. Must be an integer factor of a day. Segments start at integer multiples of the segment duration after midnight of each day, meaning a 1 hour segment length will start on the hour, regardless of when recording was started. If recording is started midway through a segment's time slot, it will record what it can to the time slot's file. (default 1h0m0s)
  -h, --help                       help for streamrecord
      --ignore-y2k                 Ignore centuries when validating template strings
  -k, --insecure-skip-verify       Accept invalid HTTPS certificates
  -l, --max-lag duration           Maximum lag, measured as the difference between the duration of the received mp3 frames and the duration of the HTTP response. If the maximum lag is exceeded, the connection will be closed and a new one will be opened. (default 10s)
  -s, --max-split-delay duration   Maximum amount of time to wait after the correct split time before actually splitting. MP3 frames can use data from a previous frame using a mechanism called the "bit reservoir". In order to cleanly split an mp3 file and avoid glitches, you need to wait for a frame that doesn't use any data from a previous frame. Streamrecord will try to split the file cleanly, but if --max-split-delay has elapsed and no clean place to split has been found, it will give up and produce an unclean break. (default 200ms)
  -f, --template string            Template for the filename, using standard strftime formatting. Must produce a unique value for every possible valid segment. (default "%FT%T.mp3")
  -t, --timeout duration           Set HTTP connect, receive timeouts. (default 2s)
```

## TODO

- --max-lag enforcement
- check for goprocess/channel leaks on GetFrame() context cancellation
- looks like a lot of streaming servers will provide a burst of buffer at the start. This is old content, if we have to reload a stream we'll be repeating ourselves! Maybe we can save the frames to a buffer, figure out a heuristic to detect this burst, and discard the burst section, minus whatever time we lost between the last successfully received frame. Maybe even use the file timestamp to preserve this behavior across process restarts
